var db = require('../utils/db')
var utils = require('../utils/index')

var getRouletteDetailSum = function (gameId, userId, gameType) {
    let whereItems = []

    whereItems.push({
        key: 'roulette_game_log.GAME_ID',
        val: gameId
    })
    whereItems.push({
        key: 'roulette_game_log.USER_ID',
        val: userId
    })
    whereItems.push({
        key: 'roulette_game_log.TYPE',
        val: gameType
    })
    whereItems.push({
        key: 'roulette_game_log.DEL_YN',
        val: 'N'
    })
    whereItems.push({
        key: 'roulette_game_total.STATUS',
        opt: '<>',
        val: 2
    })
    return db.list(db.statement('select sum(roulette_game_log.BET) as totalBet', 'roulette_game_log', 'left join roulette_game_total on roulette_game_total.ID = roulette_game_log.GAME_ID', db.lineClause(whereItems, 'and'))).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data[0].totalBet
        }
        return 0
    }).catch(res => {
        return 0
    })
}

var getBettingResult = function (userId) {
    let ret = {
        http_code: 403,
        profit: 0
    }
    var whereItems = []
    whereItems.push({
        key: 'USER_ID',
        val: userId
    })
    whereItems.push({
        key: 'DEL_YN',
        val: 'N'
    })

    return db.list(db.statement('select PROFIT from', 'roulette_game_log', '', db.lineClause(whereItems, 'and'), 'order by UPDATE_TIME desc limit 1')).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            ret.http_code = 200
            ret.profit = res.data[0].PROFIT
        }
        return ret
    }).catch(res => {
        return ret
    })
}

var getLastRolls = function () {
    var whereItems = []
    whereItems.push({
        key: 'DEL_YN',
        val: 'N'
    })
    whereItems.push({
        key: 'END',
        val: 1
    })
    return db.list(db.statement('select ROLL, HASH, TYPE from', 'roulette_game_total', '', db.lineClause(whereItems, 'and'), 'order by UPDATE_TIME desc limit 6')).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data
        }
        return null
    }).catch(res => {
        return null
    })
}

var getRollCount = function (gameType, limit) {
    var whereItems = []
    whereItems.push({
        key: 'DEL_YN',
        val: 'N'
    })
    if (gameType != '') {
        whereItems.push({
            key: 'TYPE',
            val: gameType
        })
    }
    return db.list(db.statement('select count(ID) as cnt from', 'roulette_game_total', '', db.lineClause(whereItems, 'and'), 'order by UPDATE_TIME desc ' + (limit == 0 ? '' : 'limit 0, ' + limit))).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data[0].cnt
        }
        return 0
    }).catch(res => {
        return 0
    })
}

var getRouletteGameTotalBet = function (gameId) {
    return db.list(db.statement('select sum(BET) as total_bet from', 'roulette_game_log', '', db.itemClause('GAME_ID', gameId))).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data[0].total_bet
        }
        return 0
    }).catch(res => {
        return 0
    })
}

var getRouletteGameWinPlayers = function (gameId) {
    var whereItems = []
    whereItems.push({
        key: 'GAME_ID',
        val: gameId
    })
    whereItems.push({
        key: 'WIN',
        val: 1
    })
    return db.list(db.statement('select count(*) as win_players_cnt from', 'roulette_game_log', '', db.lineClause(whereItems, 'and'))).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data[0].win_players_cnt
        }
        return 0
    }).catch(res => {
        return 0
    })
}

var getRouletteGameTotalPlayers = function (gameId) {
    var whereItems = []
    whereItems.push({
        key: 'GAME_ID',
        val: gameId
    })
    whereItems.push({
        key: 'DEL_YN',
        val: 'N'
    })

    return db.list(db.statement('select count(USER_ID) as total_players_cnt from', 'roulette_game_log', '', db.lineClause(whereItems, 'and'), "group by USER_ID")).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data[0].total_players_cnt
        }
        return 0
    }).catch(res => {
        return 0
    })
}

var existNonEndRouletteGame = function (rouletteGameId) {
    var whereItems = []
    whereItems.push({
        key: 'END',
        val: 0
    })
    whereItems.push({
        key: 'ID',
        val: rouletteGameId
    })
    return db.list(db.statement('select * from', 'roulette_game_total', '', db.lineClause(whereItems, 'and'))).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return true
        }
        return false
    }).catch(res => {
        throw res.data
    })
}
var getRouletteGameLog = function (rouletteGameId, win) {
    var whereItems = []
    whereItems.push({
        key: 'roulette_game_log.GAME_ID',
        val: rouletteGameId
    })
    whereItems.push({
        key: 'roulette_game_log.WIN',
        val: win
    })
    return db.list(db.statement('select roulette_game_log.ID, roulette_game_log.USER_ID, roulette_game_log.BET,  users.USER_NAME, users.WALLET from', 'roulette_game_log', 'left join users on users.ID=roulette_game_log.USER_ID', db.lineClause(whereItems, 'and'))).then(res => {
        if (res.success && res.data != null) {
            return res.data
        }
    }).catch(res => {
        throw res.data
    })
}
var existRouletteGameLog = function (whereItems) {
    return db.list(db.statement('select count(*) as cnt from', 'roulette_game_log', '', db.lineClause(whereItems))).then(res => {
        if (res.success && res.data != null && res.data.length > 0) {
            return res.data[0].cnt > 0
        }
        return false
    }).catch(res => {
        throw res.data
    })
}
module.exports = {
    getRouletteDetailSum,
    getBettingResult,
    getLastRolls,
    getRollCount,
    getRouletteGameLog,
    getRouletteGameTotalBet,
    getRouletteGameWinPlayers,
    getRouletteGameTotalPlayers,
    existNonEndRouletteGame,
    existRouletteGameLog
}