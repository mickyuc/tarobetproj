var express = require('express');
var app = express();
const verifyToken = require('../middleware/verify_token');

var router = express.Router();
var socketIO = require('socket.io');
var io = null;
var model = require('../model/roulette');
var commonModel = require('../model/common');
var usersModel = require('../model/users');
let rn = require('random-number');
let utils = require('../utils/index')

var rouletteGameId = 0;

// global variables
const firstTimerStart = 15000;
let currentPos = 0;
let stage = 1;
let firstTimerK = firstTimerStart;

//rolling variable
let rndNumber, curRoulette, gameType;
let gameOption = {
    min: 5,
    max: 64,
    integer: true
};
let hashServerSeed = '';

let gameIdOption = {
    min: 1,
    max: 1000000,
    integer: true
}
let BotMaker = function () {
    let imgPaths = [
        '/imgs/user_com1.jpg',
        '/imgs/user_com1.jpg',
        '/imgs/user_com1.jpg',
        '/imgs/user_com1.jpg',
        '/imgs/user_com1.jpg',
        '/imgs/user_com1.jpg',
    ];

    let betValues = [2000, 1000, 1500, 2300, 1700, 2200, 1900, 1800, 650, 1200, 1450];
    let users = [
        'Andrew',
        'Andrew Kimsoon',
        'Hence',
        'Lisa',
        'Litte',
        'Magaret',
        'Mickey',
        'Rudolf',
        'User 12',
        'User 14',
        'User 16',
        'User 100'
    ];

    let addBots = function (gameType) {
        let index = 0,
            count = utils.rnd(13) + 7;
        for (i = 0; i < count; i++) {
            setTimeout(function () {
                let data = {
                    userid: 0, // means bot
                    value: betValues[index % betValues.length],
                    username: users[utils.rnd(users.length)],
                    avatar: imgPaths[index % imgPaths.length],
                    type: gameType
                }
                io.emit('bet_message', data);
                index++;
            }, utils.rnd(100) * 100);
        }
    };

    return {
        makeBots: function () {
            addBots('PINK');
            addBots('GREEN');
            addBots('BLUE');
        }
    }
}();

let Engine = function () {
    let sql, firstTimerHandle, gameType;

    let intervalFunc = function () {
        firstTimerK -= 10;
        if (firstTimerK == 0) {
            clearInterval(firstTimerHandle);
            do_rolling();
        }
    };

    let do_rolling = async function () {
        stage = 2;

        await commonModel.updateDBbyGameID('roulette_game_total',
            rouletteGameId, [{
                key: 'STATUS',
                val: 1
            }]
        )
        rndNumber = rn(gameOption);
        if (curRoulette >= 1 && curRoulette <= 7)
            gameType = 'PINK';
        else if (curRoulette >= 8 && curRoulette <= 14)
            gameType = 'BLUE';
        else
            gameType = 'GREEN';

        io.emit('TimerEnd');
        setTimeout(function () {
            Roulette.start();
            finish_database_for_roulette();
        }, 1000);
    };

    let finish_database_for_roulette = async function () {
        await commonModel.updateDBbyCondition('roulette_game_log', [{
                key: 'TYPE',
                val: gameType
            },
            {
                key: 'GAME_ID',
                val: rouletteGameId
            }
        ], [{
            key: 'WIN',
            val: 1
        }])

        await commonModel.updateDBbyCondition('roulette_game_log', [{
                key: 'TYPE',
                val: gameType,
                opt: '<>'
            },
            {
                key: 'GAME_ID',
                val: rouletteGameId
            }
        ], [{
            key: 'WIN',
            val: 0
        }])

        const isExist = await model.existNonEndRouletteGame(rouletteGameId)
        let rouletteGameCmdResult = null;
        if (isExist) {
            rouletteGameCmdResult = await commonModel.updateDBbyGameID('roulette_game_total',
                rouletteGameId, [{
                    key: 'END',
                    val: 1
                }]
            )
        } else {
            let formatted = new Date().getTime();
            formatted = Math.round(formatted / 1000);
            rouletteGameCmdResult = await commonModel.insertToDB('roulette_game_total', [{
                    key: 'HASH',
                    val: hashServerSeed
                },
                {
                    key: 'CREATE_TIME',
                    val: formatted
                },
                {
                    key: 'UPDATE_TIME',
                    val: formatted
                },
                {
                    key: 'TYPE',
                    val: gameType
                },
                {
                    key: 'ROLL',
                    val: curRoulette
                },
                {
                    key: 'END',
                    val: 1
                },
                {
                    key: 'STATUS',
                    val: 1
                }
            ])
        }
        rouletteGameId = rouletteGameCmdResult.insertId == undefined || rouletteGameCmdResult.insertId == 0 ? rouletteGameId : rouletteGameCmdResult.insertId
    };

    let start_progress = async function () {
        firstTimerK = firstTimerStart;
        stage = 1;
        rouletteGameId = rn(gameIdOption);
        hashServerSeed = await commonModel.getGameHash('roulette', rouletteGameId)

        console.log(hashServerSeed)
        curRoulette = commonModel.rollFromHash(hashServerSeed); // we make finish roulette here ...
        io.emit('start_progress', {
            timer: "first",
            first_timer_k: firstTimerK
        });
        firstTimerHandle = setInterval(intervalFunc, 10);
        BotMaker.makeBots();
    };

    /*
      checking this point ...
      Bottom code should be executed after above code
      */
    let updateRouletteLog = async function () {
        const totalBet = await model.getRouletteGameTotalBet(rouletteGameId)
        await commonModel.updateDBbyGameID('roulette_game_total',
            rouletteGameId, [{
                key: 'TOTAL',
                val: totalBet
            }]
        )

        const winPlayers = await model.getRouletteGameWinPlayers(rouletteGameId)
        await commonModel.updateDBbyGameID('roulette_game_total',
            rouletteGameId, [{
                key: 'WINNER_PLAYERS',
                val: winPlayers
            }]
        )

        const totalPlayers = await model.getRouletteGameTotalPlayers(rouletteGameId)
        await commonModel.updateDBbyGameID('roulette_game_total',
            rouletteGameId, [{
                key: 'USERS',
                val: totalPlayers
            }]
        )

        // get admin wallet part, this part will be changed in the future

        // let formatted = new Date().getTime();
        // formatted = Math.round(formatted / 1000);

        //defeat user wallet minus
        const nonWinLogs = await model.getRouletteGameLog(rouletteGameId, 0)

        let profit = 0;
        for (let i = 0; i < nonWinLogs.length; i++) {
            let final_adding = parseFloat(nonWinLogs[i]['BET']);
            profit += final_adding;
            await usersModel.updateUsers([{
                key: 'ID',
                val: nonWinLogs[i].USER_ID
            }], [{
                    key: 'WALLET',
                    val: 'WALLET - ' + final_adding
                },
                {
                    key: 'WALLET_BLOCK',
                    val: 'WALLET_BLOCK - ' + final_adding
                }
            ])
            await commonModel.updateDBbyCondition('roulette_game_log', [{
                key: 'ID',
                val: nonWinLogs[i]['ID']
            }], [{
                key: 'PROFIT',
                val: -final_adding
            }])
        }
        //win user plus
        const winLogs = await model.getRouletteGameLog(rouletteGameId, 1)
        for (let i = 0; i < winLogs.length; i++) {
            let final_adding;
            if (gameType == 'PINK' || gameType == 'BLUE')
                final_adding = (parseFloat(result[i]['BET']) * 1);
            else
                final_adding = (parseFloat(result[i]['BET']) * 13);
            profit -= final_adding;

            await usersModel.updateUsers([{
                key: 'ID',
                val: winLogs[i].USER_ID
            }], [{
                    key: 'WALLET',
                    val: 'WALLET + ' + final_adding
                },
                {
                    key: 'WALLET_AVAILABLE',
                    val: 'WALLET_AVAILABLE + ' + (final_adding + parseFloat(winLogs[i]['BET']))
                },
                {
                    key: 'WALLET_BLOCK',
                    val: 'WALLET_BLOCK - ' + parseFloat(winLogs[i]['BET'])
                }
            ])

            await commonModel.updateDBbyCondition('roulette_game_log', [{
                key: 'ID',
                val: winLogs[i]['ID']
            }], [{
                key: 'PROFIT',
                val: final_adding
            }])
        }
        await commonModel.updateDBbyGameID('roulette_game_total',
            rouletteGameId, [{
                key: 'PROFIT',
                val: profit
            }]
        )
        // if (rouletteGameId != 0) {
        //     con.query("UPDATE admin set WALLET = WALLET + " + profit + " where LEVEL = 1 ", function (err, result) {
        //         if (err) throw err;
        //     });
        // }
        //end the database....
    };

    let finish_roll = async function () {
        stage = 3;
        let formatted = new Date().getTime();
        formatted = Math.round(formatted / 1000);
        await commonModel.updateDBbyGameID('roulette_game_total',
            rouletteGameId, [{
                    key: 'STATUS',
                    val: 2
                },
                {
                    key: 'UPDATE_TIME',
                    val: formatted
                }
            ]
        )
        updateRouletteLog();
        setTimeout(function () {
            io.emit("last_stage", {
                stage: stage,
                current_pos: currentPos,
                type: gameType,
                hash: hashServerSeed,
                stopnumber: curRoulette
            });
        }, 1000);
        setTimeout(function () {
            start_progress();
        }, 4000);
    }
    return {
        startProgress: function () {
            start_progress();
        },
        finishRoll: function () {
            finish_roll()
        }
    }
}();

let Roulette = function () {
    let pos_array = [4, 12, 14, 1, 3, 6, 8, 10, 11, 9, 7, 5, 2, 0, 13]; // value from server
    const speed = 0.1;
    let rate = 1,
        isRunUp = true,
        start_pos = 0,
        distance = 0,
        isSlowdown = false,
        maxDistance,
        slowDownStartDistance,
        roulette_bar_width = 2100;

    let reset = function () {
        start_pos = currentPos;
        isRunUp = true;
        distance = maxDistance = 0;
        rate = 1;
        isSlowdown = false;
    };

    let roll = function () {
        let speed_ = rate * rate * speed;
        if (isRunUp) {
            if (speed_ >= 30)
                isRunUp = false;
            else
                rate += 0.3;
        } else {
            if (isSlowdown) {
                if (speed_ > 4)
                    rate -= 0.1;
                else if (speed_ > 2)
                    rate -= 0.05;
                else {
                    if (speed_ < 0.5 && maxDistance - distance > 5) {
                        // we just maintain the slowest speed
                    } else
                        rate -= 0.02;
                }
                // slow down ...
            } else if (distance >= slowDownStartDistance)
                isSlowdown = true;
        }

        if (maxDistance && distance >= maxDistance) {
            currentPos += (distance - maxDistance); // now it's equal to maxDistance ...
            currentPos %= roulette_bar_width;
            reset();
            // we do finish things here
            Engine.finishRoll();
            return;
        }

        distance += speed_;
        currentPos -= speed_;

        setTimeout(roll, 20);
    };

    return {
        start: function () {
            reset();

            let len, index1, index2;

            len = distance - start_pos;
            index1 = Math.floor(len / 70) % 15;
            index2 = pos_array[curRoulette];

            if (index2 < index1)
                index2 = index2 - index1 + 15;
            else
                index2 = index2 - index1;

            if (index2 < 7)
                index2 += 15;

            if (index2 - 7 < 30)
                index2 += 15;
            rndNumber = 70 / 2;
            maxDistance = (index2 - 1) * 70 + (70 - len % 70) + rndNumber + distance - (roulette_bar_width / 2) + roulette_bar_width * 2;
            // runUpDistance is 580, slowDownDistance is 1852
            slowDownStartDistance = maxDistance - 2000 - rndNumber;
            io.emit('rolling', Roulette.roll_params());
            roll();
        },
        roll_params: function () {
            return {
                rate: rate,
                isRunUp: isRunUp,
                distance: distance,
                isSlowdown: isSlowdown,
                slowDownStartDistance: slowDownStartDistance,
                maxDistance: maxDistance,
                current_pos: currentPos
            };
        }
    };
}();

router.post('/getMyBet', verifyToken, async function (req, res) {
    const {
        token,
        userId
    } = req.body
    const res_lastGameId = await commonModel.getLastGameId('roulette_game_total', {
        isEnd: false
    });
    if (res_lastGameId.code == 403 || res_lastGameId.data.gameId == 0) {
        return res.json({
            http_code: 403,
            pink_sum: 0,
            blue_sum: 0,
            green_sum: 0
        });
    }

    const pinkSum = await model.getRouletteDetailSum(res_lastGameId.data.gameId, userId, 'PINK')
    const blueSum = await model.getRouletteDetailSum(res_lastGameId.data.gameId, userId, 'BLUE')
    const greenSum = await model.getRouletteDetailSum(res_lastGameId.data.gameId, userId, 'GREEN')

    ret = {
        http_code: 200,
        pink_sum: pinkSum,
        blue_sum: blueSum,
        green_sum: greenSum
    };
    return res.json(ret);
});

router.post('/getBettingResult', verifyToken, async function (req, res) {
    const {
        token,
        userId
    } = req.body
    const res_betting = await model.getBettingResult(userId)
    return res.json(res_betting)
});

router.post('/getRouletteHistory', verifyToken, async function (req, res) {
    // Get History(Last) Rolls
    const last_rolls = await model.getLastRolls()
    const pink_cnt = await model.getRollCount('PINK', 100)
    const blue_cnt = await model.getRollCount('BLUE', 100)
    const green_cnt = await model.getRollCount('GREEN', 100)
    return res.json({
        http_code: 200,
        history_rolls: last_rolls,
        pink_count: pink_cnt,
        green_count: green_cnt,
        blue_count: blue_cnt
    })
});

router.post('/setBetting', verifyToken, async function (req, res) {
    if (stage != 1) {
        res.json({
            http_code: 500,
            res_msg: "You can't bet because the game was already started and running."
        });
        return;
    }

    /* type, value, token from server
     * avatarmedium, avatar, userid, username : from userinfo
     * gameid, first : those should be gotten at server
     * */
    // GET USER INFO from userId data............
    const usersInfo = await usersModel.detailUsers([{
        key: 'ID',
        val: req.body.userId
    }])
    if (usersInfo == null || usersInfo.length == 0) {
        res.json({
            http_code: 403,
            res_msg: "Not authenticated user"
        });
        return;
    }

    // get "first" parameter
    let rb = {
        first: true,
        gameid: 0
    }

    const gameCount = await model.getRollCount('', 0)
    if (gameCount < 1) {
        return res.json({
            HTTP_CODE: 500,
            res_msg: 'Roulette game server has got connection problem.'
        });
    }

    // Get last game Info
    const res_lastGameLog = await commonModel.getLastGameData('roulette_game_total')
    console.log('****stage value****** ', stage);

    if (!res_lastGameLog.success || res_lastGameLog.data == null || res_lastGameLog.data.length == 0) {
        return res.json({
            http_code: 500,
            res_msg: 'Roulette game server has got connection problem.'
        })
    }
    rb.gameid = res_lastGameLog.data[0].ID;
    rb.first = res_lastGameLog.data[0].END != 0;

    console.log('#####rb first##### ', rb.first, curRoulette);

    if (rb.first) {
        if (curRoulette >= 1 && curRoulette <= 7)
            gameType = 'PINK';
        else if (curRoulette == 0)
            gameType = 'GREEN';
        else
            gameType = 'BLUE';

        let formatted = new Date().getTime();
        formatted = Math.round(formatted / 1000);
        await commonModel.insertToDB('roulette_game_total', [{
                key: 'HASH',
                val: hashServerSeed
            },
            {
                key: 'CREATE_TIME',
                val: formatted
            },
            {
                key: 'UPDATE_TIME',
                val: formatted
            },
            {
                key: 'TYPE',
                val: gameType
            },
            {
                key: 'ROLL',
                val: curRoulette
            },
            {
                key: 'END',
                val: 0
            },
            {
                key: 'TOTAL',
                val: req.body.value
            }
        ])
        rouletteGameId = rb.gameid + 1;
    } else
        rouletteGameId = rb.gameid;

    let formatted = new Date().getTime();
    formatted = Math.round(formatted / 1000);

    const existRLLog = await model.existRouletteGameLog([{
            key: 'DEL_YN',
            val: 'N'
        },
        {
            key: 'TYPE',
            val: req.body.type
        },
        {
            key: 'USER_ID',
            val: usersInfo[0].ID
        },
        {
            key: 'GAME_ID',
            val: rouletteGameId
        }
    ])

    if (existRLLog) {
        return res.json({
            http_code: 500,
            res_msg: "You can bet on only one time for each color in every round."
        });
    }
    await commonModel.insertToDB('roulette_game_log', [{
            key: 'GAME_ID',
            val: rouletteGameId
        },
        {
            key: 'USER_ID',
            val: usersInfo[0].ID
        },
        {
            key: 'BET',
            val: req.body.value
        },
        {
            key: 'TYPE',
            val: req.body.type
        },
        {
            key: 'CREATE_TIME',
            val: formatted
        },
        {
            key: 'UPDATE_TIME',
            val: formatted
        }
    ])

    await usersModel.updateUsers([{
        key: 'ID',
        val: usersInfo[0].ID
    }], [{
            key: 'WALLET_AVAILABLE',
            val: 'WALLET_AVAILABLE - ' + req.body.value
        },
        {
            key: 'WALLET_BLOCK',
            val: 'WALLET_BLOCK + ' + req.body.value
        },
        {
            key: 'UPDATE_TIME',
            val: formatted
        }
    ])

    io.emit('bet_message', {
        username: usersInfo[0].USER_NAME,
        userid: usersInfo[0].ID,
        value: req.body.value,
        type: req.body.type.replace(/'/g, ""),
        avatar: usersInfo[0].avatar,
        avatarmedium: usersInfo[0].avatar
    });

    return res.json({
        http_code: 200,
        type: req.body.type,
        wallet: usersInfo[0].WALLET_AVAILABLE - req.body.value
    });
});

var initializeGameId = async function () {
    const res_lastEndGameId = await commonModel.getLastGameId('roulette_game_total', {
        isEnd: true
    });
    if (res_lastEndGameId.code == 200 && res_lastEndGameId.data.gameId != 0) {
        rouletteGameId = res_lastEndGameId.data.gameId
        Engine.startProgress();
    }
}

var refreshGame = function (socket) {
    let params = {
        stage: stage,
        first_timer_k: firstTimerK,
        current_pos: currentPos,
    };
    // if currently rolling ?
    if (stage == 2) {
        params['roll'] = Roulette.roll_params(); // send user rolling info ...
    }

    socket.emit('refresh_message', params);
}

var initializeSocketIO = function (server) {
    io = socketIO(server)
    io.on('connection', function (socket) {

        console.log('a new roulette user connected');

        refreshGame(socket);

        if (rouletteGameId == 0) {
            // initialize game id
            initializeGameId()
        }
        Engine.startProgress();
        socket.on('disconnect', function (data) {});
        socket.on('refresh', function () {
            refreshGame(socket);
        });
    });
}
module.exports = {
    // getMyBet,
    // getBettingResult,
    // getRouletteHistory,
    // setBetting,
    router,
    initializeSocketIO
}