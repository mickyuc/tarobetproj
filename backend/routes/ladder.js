var express = require('express');
var app = express();
const verifyToken = require('../middleware/verify_token');

var router = express.Router();
var socketIO = require('socket.io');
var io = null;
var model = require('../model/ladder');
var commonModel = require('../model/common');
var usersModel = require('../model/users');
let rn = require('random-number');
let utils = require('../utils/index')

var initializeSocketIO = function (server) {
    io = socketIO(server)
    io.on('connection', function (socket) {
        console.log('a new ladder user connected');
    });
}
module.exports = {
    router,
    initializeSocketIO
}