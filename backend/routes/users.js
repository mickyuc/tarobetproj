var express = require('express');
var router = express.Router();
let moment = require('moment');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
var model = require('../model/users');
var commonModel = require('../model/common');
// var settingModel = require('../model/setting');
var config = require('../config')

const verifyToken = require('../middleware/verify_token');
router.post('/genHash', function (req, res, next) {
  let password = 'password'

  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(password, salt);

  return res.json(hash)
});

router.post('/login', async function (req, res, next) {
  let username = req.body.username,
    password = req.body.password,
    ret = {
      status: 'error',
      data: ''
    };

  if (!username || username.trim().length < 1) {
    ret.data = "Empty Username";
    return res.json(ret);
  }

  if (!password || password.trim().length < 1) {
    ret.data = "Empty Password";
    return res.json(ret);
  }
  const users = await model.detailUsers([{
    key: 'USERNAME',
    val: username
  }])
  if (users == null || users.length == 0 || !bcrypt.compareSync(password, users[0]['PASSWORD'])) {
    ret.status = 'fail'
    ret.data = "Invalid Username or Password"
    return res.json(ret)
  }
  // create a token
  let token = jwt.sign({
    id: users[0].ID,
    roles: 'normal'
  }, config.access_token_secret, {
    expiresIn: config.token_ttl * 60 * 60 * 24 * 1000
  });

  ret.status = 'success'
  ret.data = users[0]
  ret.data['jwt_token'] = token

  return res.json(ret)
});

router.post('/info', verifyToken, function (req, res, next) {
  return res.json({
    status: 'success',
    data: req.current_user
  })
});

router.post('/wallet', verifyToken, async function (req, res, next) {
  const user_id = req.current_user.id

  let nWallet = await model.getWalletInfo(user_id, 1);

  return res.json({
    status: 'success',
    data: nWallet
  })
});

router.post('/getTotalProfit', verifyToken, async function (req, res, next) {
  const user_id = req.current_user.id

  let nProfit = await model.getTotalProfit(user_id);

  return res.json({
    status: 'success',
    data: nProfit
  })
});

router.post('/getBettingData', verifyToken, async function (req, res, next) {
  const user_id = req.current_user.id

  let betData = await model.getBettingData(user_id);

  return res.json({
    status: 'success',
    data: betData
  })
});

router.post('/getUserCount', async function(req, res) {
  let userCount = await model.getUserCount('users');
  let botCount = await model.getUserCount('game_bots');

  return res.json({
    status: 'success',
    data: userCount + botCount
  })
})

router.post('/register', async function(req, res) {
  let username = req.body.username;
  let password = req.body.password;
  let email = req.body.email;

  let users = await model.detailUsers([{
    key: 'USERNAME',
    val: username
  }])
  if (users != null && users.length > 0) {
    return res.json({
      status: 'failed',
      res_msg: 'Same username exists.'
    });
  }

  if (email.length > 0) {
    users = await model.detailUsers([{
      key: 'EMAIL',
      val: email
    }])
    if (users != null && users.length > 0) {
      return res.json({
        status: 'failed',
        res_msg: 'Same e-mail exists.'
      });
    }
  }

  do {
    const chID = 'abcdefghijklmnopqrstuvwxyz1234567890'
    let szWalletID = ''
    for (let i = 0; i < parseInt(Math.random() * 6 + 6); i++) {
      szWalletID += chID[parseInt(Math.random() * chID.length)]
    }
    users = await model.detailUsers([{
      key: 'WALLET_ID',
      val: szWalletID
    }])
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(password, salt);
    if (users == null || users.length == 0) {
      let dt = new Date().getTime();
      dt = Math.round(dt / 1000);
      await commonModel.insertToDB('users', [{
          key: 'USERNAME',
          val: username
        },
        {
          key: 'PASSWORD',
          val: hash
        },
        {
          key: 'EMAIL',
          val: email
        },
        {
          key: 'CREATE_TIME',
          val: dt
        },
        {
          key: 'UPDATE_TIME',
          val: dt
        },
        {
          key: 'DEL_YN',
          val: 'N'
        },
        {
          key: 'WALLET_ID',
          val: szWalletID
        }
      ]);
      await commonModel.insertToDB('user_wallets', [{
          key: 'CREATE_TIME',
          val: dt
        },
        {
          key: 'UPDATE_TIME',
          val: dt
        },
        {
          key: 'WALLET_ID',
          val: szWalletID
        },
        {
          key: 'WALLET',
          val: 0
        }
      ]);
      return res.json({
        status: 'success'
      });
    }
  } while(users != null && users.length > 0)
})

// router.post('/sendBits', verifyToken, async function (req, res) {
//   let dt = moment(new Date()) / 1000;
//   const admin_wallet_id = 'ADMIN'

//   if (req.body.user === undefined || req.body.user === '') {
//     return res.json({
//       status: 'error',
//       data: '`user` field is not defined'
//     })
//   }
//   if (req.body.bits === undefined || isNaN(parseInt(req.body.bits))) {
//     return res.json({
//       status: 'error',
//       data: '`bits` field is not defined'
//     })
//   }
//   const users = await model.detailUsers([{
//     key: 'USERNAME',
//     val: req.body.user
//   }])
//   if (users === null || users.length === 0) {
//     return res.json({
//       status: 'error',
//       data: 'No user exists'
//     })
//   }
//   const current_users = await model.detailUsers([{
//     key: 'ID',
//     val: req.current_user.id
//   }])
//   if (current_users === null || current_users.length === 0) {
//     return res.json({
//       status: 'error',
//       data: 'No user exists'
//     })
//   }
//   const setting = await settingModel.list()
//   let fee = 1
//   if (setting.data != null && setting.data.length > 0) {
//     for (let i = 0; i < setting.data.length; i++) {
//       if (setting.data[i].VARIABLE === 'fee') {
//         fee = parseInt(setting.data[i].VALUE)
//       }
//     }
//   }
//   const recv_user = users[0]
//   const current_user = current_users[0]
//   const send_user_wallet = await model.getWalletInfo(req.current_user.id)
//   const recv_user_wallet = await model.getWalletInfo(recv_user.ID)
//   const admin_wallet_info = await commonModel.getGameDatabyCondition(
//     'user_wallets', [{
//         key: 'WALLET_ID',
//         val: admin_wallet_id
//       }
//     ]
//   )
//   let admin_wallet = 0
//   if (admin_wallet_info == null || admin_wallet_info.length == 0) {
//     admin_wallet = 0
//   } else {
//     admin_wallet = admin_wallet_info.data[0]['WALLET']
//   }

//   if (parseInt(req.body.bits) + fee > parseInt(send_user_wallet)) {
//     return res.json({
//       status: 'error',
//       data: 'Send bits is more than current bits amount'
//     })
//   }
//   await commonModel.updateDBbyCondition(
//     'user_wallets', [{
//       key: 'WALLET_ID',
//       val: current_user.WALLET_ID
//     }], [{
//         key: 'WALLET',
//         val: parseInt(send_user_wallet) - parseInt(req.body.bits) - fee
//       },
//       {
//         key: 'UPDATE_TIME',
//         val: dt
//       }
//     ]
//   )
//   await commonModel.updateDBbyCondition(
//     'user_wallets', [{
//       key: 'WALLET_ID',
//       val: recv_user.WALLET_ID
//     }], [{
//         key: 'WALLET',
//         val: parseInt(recv_user_wallet) + parseInt(req.body.bits)
//       },
//       {
//         key: 'UPDATE_TIME',
//         val: dt
//       }
//     ]
//   );
//   await commonModel.updateDBbyCondition(
//     'user_wallets', [{
//       key: 'WALLET_ID',
//       val: admin_wallet_id
//     }], [{
//         key: 'WALLET',
//         val: parseInt(admin_wallet) + fee
//       },
//       {
//         key: 'UPDATE_TIME',
//         val: dt
//       }
//     ]
//   );

//   await commonModel.insertToDB('wallet_history', [{
//       key: 'CREATE_TIME',
//       val: dt
//     },
//     {
//       key: 'UPDATE_TIME',
//       val: dt
//     },
//     {
//       key: 'FROM_WID',
//       val: current_user.WALLET_ID
//     },
//     {
//       key: 'TO_WID',
//       val: recv_user.WALLET_ID
//     },
//     {
//       key: 'FROM_PRE_W',
//       val: send_user_wallet
//     },
//     {
//       key: 'TO_PRE_W',
//       val: recv_user_wallet
//     },
//     {
//       key: 'TYPE',
//       val: 2
//     },
//     {
//       key: 'AMOUNT',
//       val: parseInt(req.body.bits)
//     }
//   ])

//   await commonModel.insertToDB('wallet_history', [{
//       key: 'CREATE_TIME',
//       val: dt
//     },
//     {
//       key: 'UPDATE_TIME',
//       val: dt
//     },
//     {
//       key: 'FROM_WID',
//       val: current_user.WALLET_ID
//     },
//     {
//       key: 'TO_WID',
//       val: admin_wallet_id
//     },
//     {
//       key: 'FROM_PRE_W',
//       val: send_user_wallet
//     },
//     {
//       key: 'TO_PRE_W',
//       val: admin_wallet
//     },
//     {
//       key: 'TYPE',
//       val: 2
//     },
//     {
//       key: 'AMOUNT',
//       val: fee
//     }
//   ])
//   return res.json({
//     status: 'success',
//     data: null
//   })
// })

router.post('/modify', verifyToken, async function (req, res, next) {
  const user_id = req.current_user.id
  if (req.body.pwd === undefined || req.body.pwd === '' || req.body.pwd.trim().length < 1) {
    return res.json({
      status: 'error',
      data: '`Pwd` field is not defined'
    })
  }
  const users = await model.detailUsers([{
    key: 'ID',
    val: user_id
  }])

  if (users == null || users.length == 0 || !bcrypt.compareSync(req.body.pwd, users[0]['PASSWORD'])) {
    return res.json({
      status: 'error',
      data: 'Invalid password'
    })
  }
  if (req.body.new_pwd !== undefined && req.body.new_pwd !== '' && req.body.new_pwd.trim().length < 6) {
    return res.json({
      status: 'error',
      data: 'New password is more than 6 letters'
    })
  }

  let setItems = []
  if (req.body.email !== undefined && req.body.email !== '' && req.body.email.trim().length >= 1) {
    setItems.push({
      key: 'EMAIL',
      val: req.body.email
    })
  }
  if (req.body.new_pwd !== undefined && req.body.new_pwd !== '' && req.body.new_pwd.trim().length >= 6) {
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(req.body.new_pwd, salt);
    setItems.push({
      key: 'PASSWORD',
      val: hash
    })
  }
  const updateUsersRes = await model.updateUsers([{
    key: 'ID',
    val: user_id
  }], setItems)

  return res.json({
    status: updateUsersRes.success ? 'success' : 'error',
    data: updateUsersRes.success ? null : 'Modifying user failed'
  })
});

module.exports = router;