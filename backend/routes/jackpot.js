var express = require('express');
var app = express();
const verifyToken = require('../middleware/verify_token');

var router = express.Router();
var socketIO = require('socket.io');
var io = null;
var model = require('../model/jackpot');
var commonModel = require('../model/common');
var usersModel = require('../model/users');
let rn = require('random-number');
let utils = require('../utils/index')

app.post('/jackpot/user_info/:userID', function (req, res) {
    let userID = req.params.userID;
    let sql = `select * from users where ID='${userID}'`;
    con.query(sql, function (err, result) {
        if (err) throw err;
        if (!result[0]) {
            return res.json({
                status: false
            });
        }
        return res.json({
            status: true,
            user: result[0]
        });
    })
});

app.post('/jackpot/ajax_get_winner/:gameID', function (req, res) {
    let gameID = req.params.gameID;
    let sql = `select * from jackpot_game where ID='${gameID}'`;
    let gameInfo;
    con.query(sql, function (err, result) {
        if (err) throw err;
        gameInfo = result[0];
        if (!gameInfo) {
            return res.json({
                status: false,
                msg: 'Invalid Game ID'
            });
        }
        finish_round(gameID, function (ferr, fresult) {
            if (ferr) throw ferr;
            return res.json(fresult);
        })
    })
});

app.get('/jackpot/ajax_round_info', function (req, res) {
    // when a new user gets into game, first he gets current game's status
    let sql = '',
        players_bet;
    cur_game(function (err, gameInfo) {
        if (err) {
            console.log(err);
            throw err;
        }
        //  player bet list
        let gameID = gameInfo['ID'];
        sql = `select USERID, sum(BET_AMOUNT) BET_AMOUNT, USERNAME, AVATAR_MEDIUM 
        from jackpot_game_log G  left join users U on U.ID=G.USERID where GAMEID='${gameInfo['ID']}' group by G.USERID, G.ID order by G.ID desc`;
        con.query(sql, function (perr, presult) {
            if (perr) throw perr;
            if (!presult[0]) {
                players_bet = [];
            } else {
                players_bet = presult;
            }
            // Get game bets
            game_bet(gameID, function (gerr, game_bet) {
                if (gerr) throw gerr;
                // get last winnner
                get_last_winner(function (lerr, lastWinner) {
                    console.log("=========last_winner Game============>", lerr);
                    if (lerr) throw lerr;
                    // get time left
                    round_time_left(gameID, function (rerr, timeLEFT) {
                        if (rerr) throw rerr;
                        gameInfo['TIME_LEFT'] = timeLEFT;
                        if (timeLEFT < 0) {
                            gameInfo['TIME_LEFT'] = 90;
                            gameInfo['STARTED'] = false;
                        } else {
                            gameInfo['STARTED'] = true;
                        }
                        res.json({
                            status: true,
                            game: gameInfo,
                            bets: game_bet,
                            players: players_bet,
                            last_winner: lastWinner
                        })
                    })
                })
            })
        })
    })
});

app.post('/jackpot/ajax_deposit', function (req, res) {
    // do this later
    if (!checkLogin(req.body.token)) {
        return res.json({
            status: true,
            msg: 'You have to login first.'
        })
    }

    getUserInfo(req.body.token, function (userInfo) {
        let gameID = req.body.game_id;
        let depositAmount = req.body.deposit_amount;

        // check if this amount is available on wallet
        if (userInfo.wallet_available < depositAmount) {
            return res.json({
                status: false,
                msg: 'Not enough wallet.'
            })
        }

        // check game winner
        let sql = `select * from jackpot_game where ID='${gameID}'`;
        con.query(sql, function (err, result) {
            if (err) return res.json({
                status: false,
                msg: 'Invalid Game Data.'
            });
            if (!result[0]) {
                return res.json({
                    status: false,
                    msg: 'Invalid Game Data.'
                });
            }
            let gameInfo = result[0];
            if (gameInfo['WINNER']) {
                return res.json({
                    status: false,
                    msg: 'Round is already finished'
                });
            }
            finish_round(gameID, function (ferr, fresult) {
                if (ferr) throw ferr;
                if (fresult['status']) {
                    return res.json({
                        status: false,
                        msg: 'Round is already finished'
                    });
                }
                // Increase block amount == new bet
                sql = `update users set WALLET_BLOCK=WALLET_BLOCK + ${depositAmount}, WALLET_AVAILABLE=WALLET_AVAILABLE - ${depositAmount} where ID='${userInfo.userid}'`;
                con.query(sql, function (nerr, nresult) {
                    if (nerr) throw nerr;
                    // update game table
                    sql = `select USERID from jackpot_game_log where GAMEID='${gameID}' and USERID='${userInfo.userid}'`;
                    con.query(sql, function (uerr, ures) {
                        if (uerr) throw uerr;
                        if (!ures[0]) {
                            // if is first bet, then increase total_players
                            /// ???????
                        }
                        sql = `update jackpot_game set TOTAL_BETTING_AMOUNT=TOTAL_BETTING_AMOUNT + ${depositAmount} where ID='${gameID}'`;
                        con.query(sql, function (ujerr, ujres) {
                            if (ujerr) throw ujerr;
                            // insert into jackpot_game_log
                            // let dt = moment(new Date()).format('YYYY-MM-DD hh:mm:ss');
                            let dt = Math.round(new Date().getTime() / 1000);
                            sql = `insert into jackpot_game_log (USERID, GAMEID, BET_AMOUNT, CREATE_TIME) values ('${userInfo.userid}', '${gameID}', ${depositAmount}, '${dt}')`;
                            con.query(sql, function (ierr, ires) {
                                if (ierr) throw ierr;
                                ////////////
                                let betInfo = {
                                    USERID: userInfo.userid,
                                    BET_AMOUNT: parseFloat(depositAmount),
                                    AVATAR_MEDIUM: userInfo.avatarmedium,
                                    USERNAME: userInfo.username
                                };
                                Jackpot.new_bet(betInfo);
                                res.json({
                                    status: true
                                });
                            })
                        })
                    });
                })

            });
        });
    });

});

app.post('/jackpot/ajax_max_amount', function (req, res) {
    if (!checkLogin(req.body.token)) {
        return res.json({
            status: false,
            msg: 'You should login first.'
        });
    }
    getUserInfo(req.body.token, function (userInfo) {
        res.json({
            status: true,
            wallet: userInfo.wallet_available
        })
    });
});

app.post('/new_deposit', function (req, res) {
    let betInfo = {
        USERID: req.body.USERID,
        BET_AMOUNT: parseFloat(req.body.BET_AMOUNT),
        AVATAR_MEDIUM: req.body.AVATAR_MEDIUM,
        USERNAME: req.body.USERNAME
    };
    Jackpot.new_bet(betInfo);
    res.json({
        'error_code': 0
    });
});


var initializeSocketIO = function (server) {
    io = socketIO(server)
    io.on('connection', function (socket) {
        console.log('a new jackpot user connected');
    });
}
module.exports = {
    router,
    initializeSocketIO
}