module.exports = {
    main_host_url: "http://localhost",
    server_port: 3001,
    roulette_port: 4200,
    jackpot_port: 4203,
    crash_port: 4202,

    mysql: '127.0.0.1',
    mysql_user: 'root',
    mysql_pwd: '',
    mysql_db: 'tarobet',

    ladder_rate: 1.95,

    access_token_secret: 'my_access_token_secret',
    token_ttl: 1
}
