import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _2d052d54 = () => interopDefault(import('..\\pages\\esports.vue' /* webpackChunkName: "pages_esports" */))
const _2341de6c = () => interopDefault(import('..\\pages\\esports\\index.vue' /* webpackChunkName: "pages_esports_index" */))
const _5766e858 = () => interopDefault(import('..\\pages\\esports\\_id.vue' /* webpackChunkName: "pages_esports__id" */))
const _660f0f0e = () => interopDefault(import('..\\pages\\esports\\_.vue' /* webpackChunkName: "pages_esports__" */))
const _2f603bf3 = () => interopDefault(import('..\\pages\\games.vue' /* webpackChunkName: "pages_games" */))
const _4707512f = () => interopDefault(import('..\\pages\\games\\index.vue' /* webpackChunkName: "pages_games_index" */))
const _7f59236c = () => interopDefault(import('..\\pages\\games\\boat_racing.vue' /* webpackChunkName: "pages_games_boat_racing" */))
const _357dccf8 = () => interopDefault(import('..\\pages\\games\\crash.vue' /* webpackChunkName: "pages_games_crash" */))
const _72215333 = () => interopDefault(import('..\\pages\\games\\jackpot.vue' /* webpackChunkName: "pages_games_jackpot" */))
const _c4de80d6 = () => interopDefault(import('..\\pages\\games\\ladder.vue' /* webpackChunkName: "pages_games_ladder" */))
const _eb820ab2 = () => interopDefault(import('..\\pages\\games\\roulette.vue' /* webpackChunkName: "pages_games_roulette" */))
const _13e2f222 = () => interopDefault(import('..\\pages\\games\\score.vue' /* webpackChunkName: "pages_games_score" */))
const _f8d87688 = () => interopDefault(import('..\\pages\\games\\_.vue' /* webpackChunkName: "pages_games__" */))
const _7af27cf8 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))
const _14cab0de = () => interopDefault(import('..\\pages\\_.vue' /* webpackChunkName: "pages__" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/esports",
    component: _2d052d54,
    children: [{
      path: "",
      component: _2341de6c,
      name: "esports"
    }, {
      path: ":id",
      component: _5766e858,
      name: "esports-id"
    }, {
      path: "*",
      component: _660f0f0e,
      name: "esports-all"
    }]
  }, {
    path: "/games",
    component: _2f603bf3,
    children: [{
      path: "",
      component: _4707512f,
      name: "games"
    }, {
      path: "boat_racing",
      component: _7f59236c,
      name: "games-boat_racing"
    }, {
      path: "crash",
      component: _357dccf8,
      name: "games-crash"
    }, {
      path: "jackpot",
      component: _72215333,
      name: "games-jackpot"
    }, {
      path: "ladder",
      component: _c4de80d6,
      name: "games-ladder"
    }, {
      path: "roulette",
      component: _eb820ab2,
      name: "games-roulette"
    }, {
      path: "score",
      component: _13e2f222,
      name: "games-score"
    }, {
      path: "*",
      component: _f8d87688,
      name: "games-all"
    }]
  }, {
    path: "/",
    component: _7af27cf8,
    name: "index"
  }, {
    path: "/*",
    component: _14cab0de,
    name: "all"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
