import Vue from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faCaretRight,
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faPaperPlane,
  faPlus,
  faMinus,
  faArrowLeft,
  faArrowRight,
  faEllipsisV,
  faTimes,
  faBars
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import SvgIcon from '@/components/SvgIcon' // svg component

library.add(faCaretRight)
library.add(faChevronDown)
library.add(faChevronUp)
library.add(faChevronLeft)
library.add(faChevronRight)
library.add(faPaperPlane)
library.add(faPlus)
library.add(faMinus)
library.add(faArrowLeft)
library.add(faArrowRight)
library.add(faEllipsisV)
library.add(faTimes)
library.add(faBars)

Vue.component('font-awesome-icon', FontAwesomeIcon)

// register globally
Vue.component('svg-icon', SvgIcon)

const req = require.context('./../assets/icons/svg', false, /\.svg$/)
const requireAll = (requireContext) => requireContext.keys().map(requireContext)
requireAll(req)
