import Vue from 'vue'

import PrettyInput from 'pretty-checkbox-vue/input.js'
import PrettyCheck from 'pretty-checkbox-vue/check.js'
import PrettyRadio from 'pretty-checkbox-vue/radio.js'

Vue.component('p-input', PrettyInput)
Vue.component('p-check', PrettyCheck)
Vue.component('p-radio', PrettyRadio)
