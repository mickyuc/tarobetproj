// import Cookies from '~/node_modules/js-cookie'

export const state = () => ({
  sidebar: {
    opened: true
  },
  rightSidebar: {
    opened: true
  },
  gameHistory: {
    id: 0,
    bust: 0,
    gameLogId: 0
  },
  curGameId: 0
})

export const getters = {
  getSidebar: (state) => state.sidebar,
  getRightSidebar: (state) => state.rightSidebar,
  getGameHistoryId: (state) => state.gameHistory.id,
  getGameHistoryBust: (state) => state.gameHistory.bust,
  getGameHistoryLogId: (state) => state.gameHistory.gameLogId,
  getCurGameId: (state) => state.curGameId
}

export const mutations = {
  TOGGLE_SIDEBAR: (state) => {
    state.sidebar.opened = !state.sidebar.opened
  },
  CLOSE_SIDEBAR: (state) => {
    state.sidebar.opened = false
  },
  SHOW_SIDEBAR: (state) => {
    state.sidebar.opened = true
  },
  TOGGLE_RIGHT_SIDEBAR: (state) => {
    state.rightSidebar.opened = !state.rightSidebar.opened
  },
  CLOSE_RIGHT_SIDEBAR: (state) => {
    state.rightSidebar.opened = false
  },
  SHOW_RIGHT_SIDEBAR: (state) => {
    state.rightSidebar.opened = true
  },
  SET_GAME_HISTORY: (state, history) => {
    state.gameHistory.id = history.GAME_ID
    state.gameHistory.bust = history.BUST
  },
  SET_GAME_HISTORY_LOG_ID: (state, id) => {
    state.gameHistory.gameLogId = id
  },
  SET_CUR_GAME_ID: (state, id) => {
    state.curGameId = id
  }
}

export const actions = {
  toggleSideBar({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar({ commit }) {
    commit('CLOSE_SIDEBAR')
  },
  showSideBar({ commit }) {
    commit('SHOW_SIDEBAR')
  },
  toggleRightSideBar({ commit }) {
    commit('TOGGLE_RIGHT_SIDEBAR')
  },
  closeRightSideBar({ commit }) {
    commit('CLOSE_RIGHT_SIDEBAR')
  },
  showRightSideBar({ commit }) {
    commit('SHOW_RIGHT_SIDEBAR')
  },
  setGameHistory({ commit }, history) {
    commit('SET_GAME_HISTORY', history)
  },
  setGameHistoryLogId({ commit }, id) {
    commit('SET_GAME_HISTORY_LOG_ID', id)
  },
  setCurGameId({ commit }, id) {
    commit('SET_CUR_GAME_ID', id)
  }
}

// export default {
//   namespaced: true,
//   state,
//   mutations,
//   actions
// }
