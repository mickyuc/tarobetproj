// https://webpack.js.org/guides/dependency-management/#requirecontext
const files = require.context('@/static/imgs/promo', true, /\.png$/)

// you do not need `import dota2 from './dota2.png'`
// it will auto require all icon from items file
const items = files.keys().reduce((items, modulePath) => {
  // set './dota2.png' => 'dota2'
  const iconName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = files(modulePath)
  items[iconName] = value
  return items
}, {})

export default {
  items
}
