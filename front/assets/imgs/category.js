// https://webpack.js.org/guides/dependency-management/#requirecontext
const iconFiles = require.context('@/static/imgs/category', true, /\.png$/)

// you do not need `import dota2 from './dota2.png'`
// it will auto require all icon from icons file
const icons = iconFiles.keys().reduce((icons, modulePath) => {
  // set './dota2.png' => 'dota2'
  const iconName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = iconFiles(modulePath)
  icons[iconName] = value
  return icons
}, {})

export default {
  icons
}
// module.exports = icons;
