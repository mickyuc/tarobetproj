// https://webpack.js.org/guides/dependency-management/#requirecontext
const flagFiles = require.context('@/static/imgs/flag', true, /\.png$/)

// you do not need `import dota2 from './dota2.png'`
// it will auto require all icon from flags file
const flags = flagFiles.keys().reduce((flags, modulePath) => {
  // set './dota2.png' => 'dota2'
  const iconName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = flagFiles(modulePath)
  flags[iconName] = value
  return flags
}, {})

export default {
  items: flags
}
// module.exports = flags;
