import * as io from 'socket.io-client'
import {
  apiGetRouletteHistory,
  apiGetBettingResult,
  apiSetBetting
} from '@/api/roulette'

export default {
  name: 'Roulette',
  data() {
    return {
      sockConnection: null
    }
  },
  mounted() {
    const self = this
    this.sockConnection = io.connect(
      this.$baseUrl.BACKEND_URL + ':' + this.$baseUrl.ROULETTE_PORT
    )

    this.sockConnection.on('refresh_message', function(data) {
      switch (data.state) {
        case 2:
          this.curState = 'WAITING ENDED'
          this.timeRemain = 0
          break
        case 3:
          this.curState = 'SPINNING | GOOD LUCK!'
          this.timeRemain = 0
          break
      }

      if (this.timerInterval) clearInterval(this.timerInterval)
      this.timerInterval = setInterval(this.timerCountingProc, 10)
    })

    this.sockConnection.on('bet_message', function a(data) {
      self.addUserData(data)
    })

    this.sockConnection.on('TimerEnd', function() {
      this.curState = 'READY FOR SPINING'
      this.timeRemain = 0
      setTimeout(function() {
        this.curState = 'SPINNING | GOOD LUCK!'
      }, 1000)
    })

    this.sockConnection.on('rolling', function(data) {
      self.startRolling(data)
    })

    this.sockConnection.on('last_stage', function(data) {
      self.stopRolling(data)
    })

    this.sockConnection.on('start_progress', function(data) {
      self.startCounting(data)
    })
  },
  methods: {
    stopRolling(data) {
      this.curRollPos = data.current_pos
      this.refreshRollbar()
      this.aryHistory.push({
        stopNumber: data.stopnumber,
        type: data.type,
        hash: data.hash
      })
      if (this.aryHistory.length > 7) this.aryHistory.splice(0, 1)

      this.curState = 'WAIT SPINNING'
      this.timeRemain = 15.0
      this.getRouletteGameHistory()
      this.getBettingResult()
      setTimeout(function waitBet() {
        this.curState = 'WAITING YOUR BETS'
      }, 1000)
    },
    async getRouletteGameHistory() {
      const response = await apiGetRouletteHistory()

      if (response.HTTP_CODE === 200) {
        const gameInfo = {
          cntGreen: response.GREEN_COUNT,
          cntBlue: response.BLUE_COUNT,
          cntPink: response.PINK_COUNT,
          rollList: response.HISTORY_ROLLS
        }
        this.setHistoryInfo(gameInfo)
      }
    },
    async getBettingResult() {
      const userData = {
        userId: this.userId
      }
      const response = await apiGetBettingResult(userData)

      if (response.HTTP_CODE === 200) {
        if (response.PROFIT > 0) {
          if (this.$refs.optOnWin.value === 'Return to base bet')
            this.autoBettingValue = this.autoBaseBettingValue
          else if (this.$refs.optOnWin.value === 'Multiply by')
            this.autoBettingValue *= 2
          else if (this.$refs.optOnWin.value === 'Add')
            this.autoBettingValue += this.autoBaseBettingValue
          else if (this.$refs.optOnWin.value === 'Subtract')
            this.autoBettingValue -= this.autoBaseBettingValue
        }
        if (response.PROFIT < 0) {
          if (this.$refs.optOnLoss.value === 'Return to base bet')
            this.autoBettingValue = this.autoBaseBettingValue
          else if (this.$refs.optOnLoss.value === 'Multiply by')
            this.autoBettingValue *= 2
          else if (this.$refs.optOnLoss.value === 'Add')
            this.autoBettingValue += this.autoBaseBettingValue
          else if (this.$refs.optOnLoss.value === 'Subtract')
            this.autoBettingValue -= this.autoBaseBettingValue
        }
      }

      if (this.autoBettingValue < this.autoStopBalanceLessValue)
        this.autoBettingValue = this.autoStopBalanceLessValue
      if (this.autoBettingValue > this.autoStopBalanceMoreValue)
        this.autoBettingValue = this.autoStopBalanceMoreValue
    },
    setHistoryInfo(infoHist) {
      if (infoHist) this.gameHistory = infoHist
      else {
        this.gameHistory.cntGreen = 0
        this.gameHistory.cntBlue = 0
        this.gameHistory.cntPink = 0
        this.gameHistory.rollList = 0
      }
    },
    async setBet() {
      let betValue = this.bettingValue
      if (this.$refs.betMode.btnState) betValue = this.autoBettingValue

      const betData = {
        userId: this.userId,
        type: this.betTypeClass,
        value: betValue
      }
      this.isBetFinished = true
      await apiSetBetting(betData)
    },
    addUserData(data) {
      if (data.type === 'BLUE') {
        this.updateMaxBetUser(this.userTopBlue, data)

        this.aryUserInfoBlue.push({
          username: data.username,
          diamondCount: 0,
          bettingval: data.value
        })
        this.aryUserInfoBlue.sort(function(a, b) {
          if (a.bettingval > b.bettingval) return -1
          else if (a.bettingval < b.bettingval) return 1
          else return 0
        })
      } else if (data.type === 'GREEN') {
        this.updateMaxBetUser(this.userTopGreen, data)

        this.aryUserInfoGreen.push({
          username: data.username,
          diamondCount: 0,
          bettingval: data.value
        })
        this.aryUserInfoGreen.sort(function(a, b) {
          if (a.bettingval > b.bettingval) return -1
          else if (a.bettingval < b.bettingval) return 1
          else return 0
        })
      } else if (data.type === 'PINK') {
        this.updateMaxBetUser(this.userTopPink, data)

        this.aryUserInfoPink.push({
          username: data.username,
          diamondCount: 0,
          bettingval: data.value
        })
        this.aryUserInfoPink.sort(function(a, b) {
          if (a.bettingval > b.bettingval) return -1
          else if (a.bettingval < b.bettingval) return 1
          else return 0
        })
      }
    },
    updateMaxBetUser(maxBetUserInfo, data) {
      if (maxBetUserInfo.bettingval < data.value) {
        maxBetUserInfo.bettingval = data.value
        maxBetUserInfo.topname = data.username
      }
    },
    startCounting(data) {
      if (data.timer === 'first') {
        this.timeRemain = data.first_timer_k / 1000
        this.timeStart = new Date().getTime()
        if (this.timerInterval) clearInterval(this.timerInterval)
        this.timerInterval = setInterval(this.timerCountingProc, 20)
        this.curState = 'COUNTING'

        this.aryUserInfoBlue.splice(0, this.aryUserInfoBlue.length)
        this.aryUserInfoGreen.splice(0, this.aryUserInfoGreen.length)
        this.aryUserInfoPink.splice(0, this.aryUserInfoPink.length)
      }
    },
    startRolling(data) {
      this.curState = 'SPINNING | GOOD LUCK!'
      if (this.timerRolling) clearInterval(this.timerRolling)
      this.rollingParams.rate = data.rate
      this.rollingParams.isRunUp = data.isRunUp
      this.rollingParams.distance = data.distance
      this.rollingParams.isSlowdown = data.isSlowdown
      this.rollingParams.maxDistance = data.maxDistance
      this.rollingParams.slowDownStartDistance = data.slowDownStartDistance

      this.timerRolling = setInterval(this.timerRollingProc, 20)
      this.timerRollingProc()
    }
  }
}
