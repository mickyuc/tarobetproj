import AxiosRequest from '@/api/AxiosRequest'

export function apiGetRouletteHistory() {
  const request = AxiosRequest.roulette()
  return request({
    url: '/Roulette/getRouletteHistory',
    method: 'post'
  })
}

export function apiGetBettingResult(data) {
  const request = AxiosRequest.roulette()
  return request({
    url: '/Roulette/getBettingResult',
    method: 'post',
    data
  })
}

export function apiSetBetting(data) {
  const request = AxiosRequest.roulette()
  return request({
    url: '/Roulette/setBetting',
    method: 'post',
    data
  })
}
