export default (res, response) => {
  return res
    .status((response || {}).status || 500)
    .json(((response || {}).data || {}).Messages || {})
}
