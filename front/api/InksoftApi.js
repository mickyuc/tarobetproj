import axios from 'axios'
import Cookies from '~/node_modules/js-cookie'

const tokenKey = 'token_key'

const onOk = (response) => {
  return response.data
}

const onError = (err) => {
  return Promise.reject(err.response)
}

axios.defaults.baseURL =
  process.env.BACKEND_URL + ':' + process.env.BACKEND_PORT
axios.defaults.prefix = process.env.BACKEND_URL + ':' + process.env.BACKEND_PORT
axios.defaults.timeout = 5000
axios.defaults.headers.common.Authorization =
  'bearer ' + Cookies.get(tokenKey) || ''
axios.interceptors.response.use(onOk, onError)

class InksoftApi {
  static temp() {
    return axios.post('/users')
  }

  static login(payload) {
    return axios.post('/User/auth/login', payload)
  }

  static userInfo() {
    return axios.post('/User/auth/info')
  }

  static sendRouletteRequest(url, data = false) {
    return axios.post(
      process.env.BACKEND_URL + ':' + process.env.ROULETTE_PORT + url,
      data
    )
  }

  static sendCrashRequest(url, data = false) {
    return axios.post(
      process.env.BACKEND_URL + ':' + process.env.CRASH_PORT + url,
      data
    )
  }
}

export default InksoftApi
