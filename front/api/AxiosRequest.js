import axios from 'axios'
import Cookies from '~/node_modules/js-cookie'
const tokenKey = 'token_key'

const onOk = (response) => {
  return response.data
}

const onError = (err) => {
  return Promise.reject(err.response)
}

axios.defaults.baseURL =
  'http://' + process.env.APP_HOST + ':' + process.env.APP_PORT + '/'
axios.defaults.prefix =
  'http://' + process.env.APP_HOST + ':' + process.env.APP_PORT + '/'
axios.defaults.timeout = 5000
// axios.defaults.headers.common.Authorization = 'bearer ' + Cookies.get(tokenKey) || ''
axios.interceptors.response.use(onOk, onError)

class AxiosRequest {
  static user() {
    axios.defaults.headers.common.Authorization =
      'bearer ' +
      (Cookies.get(tokenKey) === undefined ? '' : Cookies.get(tokenKey))
    // baseURL: process.env.BACKEND_URL + ':' + process.env.BACKEND_PORT,
    const service = axios.create({
      timeout: 5000
    })
    service.interceptors.response.use(onOk, onError)
    return service
  }

  static roulette() {
    axios.defaults.headers.common.Authorization =
      'bearer ' +
      (Cookies.get(tokenKey) === undefined ? '' : Cookies.get(tokenKey))
    // baseURL: process.env.BACKEND_URL + ':' + process.env.ROULETTE_PORT,
    const service = axios.create({
      timeout: 5000
    })
    service.interceptors.response.use(onOk, onError)
    return service
  }

  static crash() {
    axios.defaults.headers.common.Authorization =
      'bearer ' +
      (Cookies.get(tokenKey) === undefined ? '' : Cookies.get(tokenKey))
    // baseURL: process.env.BACKEND_URL + ':' + process.env.CRASH_PORT,
    const service = axios.create({
      timeout: 5000
    })
    service.interceptors.response.use(onOk, onError)
    return service
  }
}

export default AxiosRequest
