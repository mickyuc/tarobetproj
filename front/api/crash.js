import AxiosRequest from '@/api/AxiosRequest'

export function apiBet(data) {
  const request = AxiosRequest.crash()
  return request({
    url: '/Crash/bet',
    method: 'post',
    data
  })
}

export function apiCashOut(data) {
  const request = AxiosRequest.crash()
  return request({
    url: '/Crash/cashOut',
    method: 'post',
    data
  })
}
