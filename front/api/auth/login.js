import InksoftApi from '../InksoftApi'
import handleApiError from '../handleApiError'

export default async (req, res) => {
  try {
    const {
      Data
    } = await InksoftApi.login(req.body)

    res.json(Data)
  } catch (err) {
    handleApiError(res, err)
  }
}
