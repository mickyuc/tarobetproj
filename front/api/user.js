import AxiosRequest from '@/api/AxiosRequest'

export function login(data) {
  const request = AxiosRequest.user()
  return request({
    url: '/User/auth/login',
    method: 'post',
    data
  })
}

export function userInfo(data) {
  const request = AxiosRequest.user()
  return request({
    url: '/User/auth/info',
    method: 'post'
  })
}
